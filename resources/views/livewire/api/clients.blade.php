<div>


    <div>
        {{-- bg-white overflow-hidden shadow-xl sm:rounded-lg --}}
    
    
        <div class="flex fle-col max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="w-full m-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
    
                <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
    
                    <div class="shadow-xl overflow-hidden border-b border-gray-200 sm:rounded-lg">
    
                        <div class="px-8 py-4 flex items-center">


                            <div class="mb-3 xl:w-96">
                                <select wire:model='selec' class="flex-1 mr-4">
                                    
                                    <option value="0">Cliente</option>
                                    @foreach ($datas as $cod)
                                        
                                    
                                        <option value="{{$cod->code}} "> {{$cod->code}} </option>

                                    @endforeach

                                   
                                </select>
                             </div>

                             <input wire:model='fech' class="flex-1 mr-4 py-2" type="date" name="" id="">

                             <x-jet-danger-button class="mr-4 px-9 py-3" wire:click='res'>
                                 Resetear
                             </x-jet-danger-button>

                            </div>
  
                                 
                        </div>
    
                                @if ($dataClientes->count())
                                    <table class="w-full divide-y divide-gray-200">
                                <thead class="bg-gray-50">
                                    <tr>
                                        <th scope="col" wire:click='order("id")'
                                            class="cursor-pointer px-6 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Client
    
                                        
    
                                        </th>
                                        <th scope="col" wire:click='order("title")'
                                            class="cursor-pointer px-6 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            Chamadas total
    
    
                                        </th>
                                        <th scope="col" wire:click='order("content")'
                                            class="cursor-pointer px-6 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            chamadas nao atendida
    
    
                                        </th>
    
                                        <th scope="col"
                                            class="px-6 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                            ocorrencias total
                                        </th>

                                        <th scope="col"
                                        class="px-6 py-3 border-b-2 border-gray-200 bg-gray-100 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">
                                        ocorrencias abordagem
                                    </th>
    
    
                                    </tr>
                                </thead>
                         <tbody>
                            @foreach ($dataClientes as $item)
                            <tr>
                                <td class="px-6 py-4">

                                    <div class="text-sm text-gray-900">
                                        <p class="text-gray-900 whitespace-no-wrap">{{ $item->code }}</p>
                                    </div>

                                    <div class="text-sm text-gray-900">
                                        <p class="text-gray-900 whitespace-no-wrap">Data</p>
                                    </div>

                                    <div class="text-sm text-gray-900">
                                        <p class="text-gray-900 whitespace-no-wrap">{{ $item->data }}</p>
                                    </div>

                                </td>
                                <td class="px-6 py-4">

                                    <div class="text-sm text-gray-900">
                                        <p class="text-gray-900 whitespace-no-wrap">{{ $item->chamadas_total }}</p>
                                    </div>

                                </td>
                                <td class="px-6 py-4">

                                    <div class="text-sm text-gray-900">
                                        <p class="text-gray-900 whitespace-no-wrap">{{ $item->chamadas_nao_atendida }}</p>
                                    </div>

                                </td>

                                <td class="px-6 py-4">

                                    <div class="text-sm text-gray-900">
                                        <p class="text-gray-900 whitespace-no-wrap">{{ $item->ocorrencias_total }}</p>
                                    </div>

                                </td>

                                <td class="px-6 py-4">

                                    <div class="text-sm text-gray-900">
                                        <p class="text-gray-900 whitespace-no-wrap">{{ $item->ocorrencias_abordagem }}</p>
                                    </div>

                                </td>
                            </tr>
                        @endforeach
    
                                </tbody>
                            </table>
                         @else
                            <div class="w-full px-6 py-4">
                                Não há registros
                            </div>
                        @endif 
                           
                    </div>
                </div>
            </div>
        </div>
    
    
    
    
    </div>

</div>

