<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client', function (Blueprint $table) {
            $table->id();
            $table->date('data');
            $table->time('hora');
            $table->string('code');
            $table->string('chamadas_total');
            $table->string('chamadas_falha_operadora');
            $table->string('chamadas_telefone_incorreto');
            $table->string('chamadas_nao_atendida');
            $table->string('chamadas_atendimento_maquina' );
            $table->string('chamadas_atendimento_humano');
            $table->string('chamadas_abandono_pre_fila');
            $table->string('chamadas_abandono_fila');
            $table->string('chamadas_atendimento_pa');
            $table->string('ocorrencias_total');
            $table->string('ocorrencias_sem_contato');
            $table->string('ocorrencias_com_contato');
            $table->string('ocorrencias_abordagem');
            $table->string('ocorrencias_fechamento');
            $table->foreignId('geral_id')->constrained('geral');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client');
    }
};
