<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geral', function (Blueprint $table) {
            $table->id();
            
            $table->date('data');
            $table->time('hora');
            $table->string('chamadas_total')->nullable();
            $table->string('chamadas_falha_operadora')->nullable();
            $table->string('chamadas_telefone_incorreto')->nullable();
            $table->string('chamadas_nao_atendida')->nullable();
            $table->string('chamadas_atendimento_maquina')->nullable();
            $table->string('chamadas_atendimento_humano')->nullable();
            $table->string('chamadas_abandono_pre_fila')->nullable();
            $table->string('chamadas_abandono_fila')->nullable();
            $table->string('chamadas_atendimento_pa')->nullable();
            $table->string('ocorrencias_total')->nullable();
            $table->string('ocorrencias_sem_contato')->nullable();
            $table->string('ocorrencias_com_contato')->nullable();
            $table->string('ocorrencias_abordagem')->nullable();
            $table->string('ocorrencias_fechamento')->nullable();
            
            $table->timestamps();
        });
    }

 /*    data: "2022-05-20",
hora: "23:59:59",
clientes: "cliente01,cliente02,cliente03,cliente04,cliente05,cliente06,cliente07,cliente08",
contas_quantidade: 8,
chamadas_total: 46379,
chamadas_falha_operadora: 18127,
chamadas_telefone_incorreto: 4593,
chamadas_nao_atendida: 3800,
chamadas_atendimento_maquina: 14847,
chamadas_atendimento_humano: 3966,
chamadas_abandono_pre_fila: 1046,
chamadas_abandono_fila: 965,
chamadas_atendimento_pa: 3001,
ocorrencias_total: 4189,
ocorrencias_sem_contato: 1887,
ocorrencias_com_contato: 2302,
ocorrencias_abordagem: 1449,
ocorrencias_fechamento: 9, */

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geral');
    }
};
