<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    use HasFactory;

    protected $table = "client";

    protected $fillable = [
        'data',
        'hora',
        'geral_id',
        'code',
        'chamadas_total',
        'chamadas_falha_operadora',
        'chamadas_telefone_incorreto',
        'chamadas_nao_atendida',
        'chamadas_atendimento_maquina',
        'chamadas_atendimento_humano',
        'chamadas_abandono_pre_fila',
        'chamadas_abandono_fila',
        'chamadas_atendimento_pa',
        'ocorrencias_total',
        'ocorrencias_sem_contato',
        'ocorrencias_com_contato',
        'ocorrencias_abordagem',
        'ocorrencias_fechamento',

    ];
}
