<?php

namespace App\Http\Livewire\Api;

use App\Models\Geral;
use App\Models\Client;
use Livewire\Component;
use Illuminate\Support\Facades\Http;

class Consult extends Component
{

    
    public function render()
    {

        $response = Http::get('https://www.ibridge.com.br/dados.json');
        $data =  $response->json();

  
        foreach ($data as $key => $value) {

            $geral = $value['geral'];
           
            //aca el updateOrCreate geral
           $dataGeral = Geral::updateOrCreate([
                'data' => $geral['data'],
                'hora' => $geral['hora'],

           ]);

           //declaras las variables a sumar
           $chamadasTotal = 0;
           $chamadasFalhaOperadora = 0;
           $chamadasTelefoneIncorreto = 0;
           $chamadasNaoAtendida = 0;
           $chamadasAtendimentoMaquina = 0;
           $chamadasAtendimentoHumano = 0;
           $chamadasAbandonoPreFila = 0;
           $chamadasAbandonoFila = 0;
           $chamadasAtendimentoPa = 0;
           $ocorrenciasTotal = 0;
           $ocorrenciasSemContato = 0;
           $ocorrenciasComContato = 0;
           $ocorrenciasAbordagem = 0;
           $ocorrenciasFechamento = 0;
            //aca recorres los clientes
            foreach ($value['clientes'] as $key => $client)
            {
               //aca el updateOrCreate de clientes  Creas la migracion y el modelo de cliente con todos sus campos y agregas el campo code para que insertes el codigo del cliente
             
               Client::updateOrCreate(
                [
                    'data' => $client['data'],
                    'hora' => $client['hora'],
                    'geral_id' => $dataGeral->id,
                    'code' => $key,
               ],
               [
                    //aca agregas toda la informacion del  cliente que falta
                    'chamadas_total' =>  $client['chamadas_total'],
                    'chamadas_falha_operadora' => $client['chamadas_falha_operadora'],
                    'chamadas_telefone_incorreto' => $client['chamadas_telefone_incorreto'],
                    'chamadas_nao_atendida' => $client['chamadas_nao_atendida'],
                    'chamadas_atendimento_maquina' => $client['chamadas_atendimento_maquina'],
                    'chamadas_atendimento_humano' => $client['chamadas_atendimento_humano'],
                    'chamadas_abandono_pre_fila' =>  $client['chamadas_abandono_pre_fila'],
                    'chamadas_abandono_fila' => $client['chamadas_abandono_fila'],
                    'chamadas_atendimento_pa' => $client['chamadas_atendimento_pa'],
                    'ocorrencias_total' => $client['ocorrencias_total'],
                    'ocorrencias_sem_contato' => $client['ocorrencias_sem_contato'],
                    'ocorrencias_com_contato' => $client['ocorrencias_com_contato'],
                    'ocorrencias_abordagem' => $client['ocorrencias_abordagem'],
                    'ocorrencias_fechamento' =>  $client['ocorrencias_fechamento'] ,

               ]);

            //aca sumas los campos de las variales clientes

            $chamadasTotal = $chamadasTotal + $client['chamadas_total'];
            $chamadasFalhaOperadora = $chamadasFalhaOperadora  + $client['chamadas_falha_operadora'];
            $chamadasTelefoneIncorreto =  $chamadasTelefoneIncorreto + $client['chamadas_telefone_incorreto'];
            $chamadasNaoAtendida = $chamadasNaoAtendida  + $client['chamadas_nao_atendida'];
            $chamadasAtendimentoMaquina = $chamadasAtendimentoMaquina + $client['chamadas_atendimento_maquina'];
            $chamadasAtendimentoHumano = $chamadasAtendimentoHumano + $client['chamadas_atendimento_humano'];
            $chamadasAbandonoPreFila = $chamadasAbandonoPreFila  + $client['chamadas_abandono_pre_fila'];
            $chamadasAbandonoFila =  $chamadasAbandonoFila + $client['chamadas_abandono_fila'];
            $chamadasAtendimentoPa = $chamadasAtendimentoPa + $client['chamadas_atendimento_pa'];
            $ocorrenciasTotal = $ocorrenciasTotal + $client['ocorrencias_total'];
            $ocorrenciasSemContato = $ocorrenciasSemContato + $client['ocorrencias_sem_contato'];
            $ocorrenciasComContato = $ocorrenciasComContato + $client['ocorrencias_com_contato'];
            $ocorrenciasAbordagem = $ocorrenciasAbordagem + $client['ocorrencias_abordagem'];
            $ocorrenciasFechamento =  $ocorrenciasFechamento + $client['ocorrencias_fechamento'];

            }


            //aca la actualizacion de $geral 

            $dataGeral->update([
                'chamadas_total' =>  $chamadasTotal,
                'chamadas_falha_operadora' => $chamadasFalhaOperadora,
                'chamadas_telefone_incorreto' => $chamadasTelefoneIncorreto,
                'chamadas_nao_atendida' => $chamadasNaoAtendida,
                'chamadas_atendimento_maquina' => $chamadasAtendimentoMaquina,
                'chamadas_atendimento_humano' => $chamadasAtendimentoHumano,
                'chamadas_abandono_pre_fila' =>  $chamadasAbandonoPreFila,
                'chamadas_abandono_fila' => $chamadasAbandonoFila,
                'chamadas_atendimento_pa' => $chamadasAtendimentoPa,
                'ocorrencias_total' => $ocorrenciasTotal,
                'ocorrencias_sem_contato' => $ocorrenciasSemContato,
                'ocorrencias_com_contato' => $ocorrenciasComContato,
                'ocorrencias_abordagem' => $ocorrenciasAbordagem,
                'ocorrencias_fechamento' =>  $ocorrenciasFechamento
            ]);
        }

        $dataGerals = Geral::all();
           //aca retorna la vista con los datos insertados en la Bd
           /* return $dataGeral; */
        $dataClient = Client::all();

           $c=Client::distinct('code')->get('code');
        return view('livewire.api.consult');
    }
}
