<?php

namespace App\Http\Livewire\Api;

use App\Models\Client;
use App\Models\Geral;
use Livewire\Component;

class Clients extends Component
{
    public $selec = '';
    public $dat = '2022-05-20';
    public $fech = '';

    //cliente04

    public function res(){
        $this->reset([
            'selec',
            'fech'
        ]);
    }
    public function render()
    {
        $dataClient = Client::all();

        $datas = Client::distinct('code')->get('code');

       


        if ($this->selec > 0 || $this->fech > 0) {
            # code...
            $dataClientes = Client::where('code', '=', $this->selec)->orWhere('data', '=', $this->fech)->get();

            return view('livewire.api.clients', compact('dataClientes','datas'));

            
            
        } else {

            $dataClientes = Client::all();
            return view('livewire.api.clients', compact('dataClientes', 'datas'));
           
        }
           
       

    }
}
